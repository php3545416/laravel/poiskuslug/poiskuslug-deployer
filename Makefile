include .env

DOCKER_COMPOSE_BASE_COMMAND = docker compose -p ${PROJECT_NAME} -f docker-compose.yml
DOCKER_COMPOSE_COMMAND = ${DOCKER_COMPOSE_BASE_COMMAND}

run: ## run project
	@${DOCKER_COMPOSE_COMMAND} up -d

stop: ## stop project
	@${DOCKER_COMPOSE_COMMAND} stop
